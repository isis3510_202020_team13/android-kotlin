package com.example.moviles_project

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class DisorderViewModel: ViewModel() {
    var database = FirebaseDatabase.getInstance()
    var dis = database.getReference("Disorders");
    private val disorders: MutableLiveData<MutableList<Disorder>> by lazy {
        MutableLiveData<MutableList<Disorder>>().also {
            loadDisorder()
        }
    }

    fun getDisorders(): LiveData<MutableList<Disorder>> {
        return disorders
    }

    private fun loadDisorder(){
        dis.addValueEventListener( object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var list:MutableList<Disorder> = mutableListOf<Disorder>()
                if (dataSnapshot.exists()) run {
                    val data = dataSnapshot.children!!
                    data.forEach{
                        var dat= it.getValue() as Map<String,Any>
                        var dis = Disorder(dat["Name"].toString(),dat["Description"].toString(),dat["Symptoms"].toString(),dat["Treatment"].toString())
                        list.add(dis)
                        disorders.postValue(list)

                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Failed to read value
            }
        })

    }
}