package com.example.moviles_project

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.item_doctor.view.*
import kotlinx.android.synthetic.main.item_hospital.view.*
import kotlin.math.roundToInt
import kotlin.math.roundToLong
import java.math.RoundingMode
import java.text.DecimalFormat

class HospitalAdapter (private val mContext: Context, private val listHos:List<Hospital>) : ArrayAdapter<Hospital>(mContext,0,listHos) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layout = LayoutInflater.from(mContext).inflate(R.layout.item_hospital, parent, false)
        val hospital = listHos[position]
        layout.hospital_name.text = hospital.name
        layout.hospital_address.text = hospital.address
        layout.hospital_rating_text.text = hospital.rating.toString()
        val df = DecimalFormat("#.###")
        df.roundingMode = RoundingMode.CEILING
        layout.distance_to.text = df.format(hospital.Distance).toString() + " KM"
        return layout
    }
}
