package com.example.moviles_project

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ListView
import androidx.lifecycle.Observer
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [medications.newInstance] factory method to
 * create an instance of this fragment.
 */
class medications : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private var param1: String? = null
    private var param2: String? = null
    val medViewModel : MedicationViewModel = MedicationViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        firebaseAnalytics = Firebase.analytics
        val root = inflater.inflate(R.layout.fragment_medications, container, false)

        medViewModel.getmedications().observe((activity as MainActivity),Observer<MutableList<Medication>>{
            val list = root.findViewById<View>(R.id.meds_list) as ListView
           var adapter: MedsAdapter =MedsAdapter(requireContext(),it)
            list.adapter = adapter
            list.setOnItemClickListener{parentFragment,view, position, id ->
                val intent = Intent(context,MedicationActivity::class.java)
                intent.putExtra("medication",it[position])
                startActivity(intent)
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                    var  name = "medication" + it[position].nameShort
                    param(FirebaseAnalytics.Param.ITEM_ID, name)
                    param(FirebaseAnalytics.Param.ITEM_NAME, name)
                    param(FirebaseAnalytics.Param.CONTENT_TYPE, "button")
                }
            }
        })

        return root
    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment medications.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            medications().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}