  package com.example.moviles_project

import android.content.Intent
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase

  // TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/*
 * A simple [Fragment] subclass.
 * Use the [fragment_hospitals.newInstance] factory method to
 * create an instance of this fragment.
 */
class fragment_hospitals : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var hospitalViewModel:HospitalViewModel = HospitalViewModel()
    private lateinit var firebaseAnalytics: FirebaseAnalytics


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         param1 = arguments?.getString("ARG_PARAM1")
        param2 = arguments?.getString("ARG_PARAM2")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        firebaseAnalytics = Firebase.analytics
        val root = inflater.inflate(R.layout.fragment_hospitals, container, false)
        val latitudeUser =(activity as MainActivity).latitude
        val longitudeUser =(activity as MainActivity).longitude
        val loc1 = Location("")
        loc1.latitude = latitudeUser as Double
        loc1.longitude = longitudeUser as Double

        hospitalViewModel.getHospitals().observe((activity as MainActivity),Observer<MutableList<Hospital>>{
            val list = root.findViewById<View>(R.id.hospitals_list) as ListView
            var hosList = mutableListOf<Hospital>()
            it.forEach(){hospital: Hospital ->
                val loc2 = Location("")
                loc2.latitude = hospital.latitude
                loc2.longitude = hospital.longitude
                val distanceInMeters = loc1.distanceTo(loc2)
                val hospitalDis = (distanceInMeters/1000)
                hospital.Distance = hospitalDis
                if(hospital.Distance <= 4.000){
                    hosList.add(hospital)
                }
            }
            var adapter: HospitalAdapter =HospitalAdapter(requireContext(),hosList)
            list.adapter = adapter
            list.setOnItemClickListener{parentFragment,view, position, id ->
                Log.d("OPRIMIO","Oprimio")
                val intent = Intent(Intent.ACTION_CALL);
                var phone = hosList[position].phone
                intent.data = Uri.parse("tel:$phone")
                var  name = "hos_"+hosList[position].name+"_called"
                startActivity(intent)
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                    param(FirebaseAnalytics.Param.ITEM_ID, name)
                    param(FirebaseAnalytics.Param.ITEM_NAME, name)
                    param(FirebaseAnalytics.Param.CONTENT_TYPE, "hospital")
                }
            }
        })

        return root }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment fragment_hospitals.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            fragment_hospitals().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

}