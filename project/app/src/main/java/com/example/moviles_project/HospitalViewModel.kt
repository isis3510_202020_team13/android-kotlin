package com.example.moviles_project

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class HospitalViewModel:ViewModel()
{
    var database = FirebaseDatabase.getInstance()
    var hos = database.getReference("Hospitals");
    private val hospitals: MutableLiveData<MutableList<Hospital>> by lazy {
        MutableLiveData<MutableList<Hospital>>().also {
            loadHospitals()
        }
    }
    fun getHospitals(): LiveData<MutableList<Hospital>> {
        return hospitals
    }
    private fun loadHospitals(){
        hos.addValueEventListener( object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var list:MutableList<Hospital> = mutableListOf<Hospital>()
                if (dataSnapshot.exists()) run {
                    val data = dataSnapshot.children!!
                    data.forEach{
                        var dat= it.getValue() as Map<String,Any>
                        val loc1 = Location("")
                        loc1.latitude = dat["Latitude"] as Double
                        loc1.longitude = dat["Longitude"] as Double

                        val loc2 = Location("")
                        loc2.latitude = dat["Latitude"] as Double
                        loc2.longitude = dat["Longitude"] as Double
                        val distanceInMeters = loc1.distanceTo(loc2)
                        var hos = Hospital(dat["Name"] as String,dat["Address"] as String,dat["Rating"] as Number,dat["Latitude"] as Double,dat["Longitude"] as Double,distanceInMeters,dat["Phone"] as String)
                        list.add(hos)
                        hospitals.postValue(list)
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Failed to read value
            }
        })
    }


}