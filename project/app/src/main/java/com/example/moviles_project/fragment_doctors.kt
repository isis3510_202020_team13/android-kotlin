package com.example.moviles_project

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ListView
import androidx.lifecycle.Observer
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


val docViewModel : DoctorViewModel = DoctorViewModel()



/**
 * A simple [Fragment] subclass.
 * Use the [fragment_doctors.newInstance] factory method to
 * create an instance of this fragment.
 */
class fragment_doctors : Fragment() {
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var psychiatricDocs:MutableList<Doctor> = mutableListOf<Doctor>()
    var psycologyDocs:MutableList<Doctor> = mutableListOf<Doctor>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        firebaseAnalytics = Firebase.analytics
        val root = inflater.inflate(R.layout.fragment_doctors, container, false)
        psychiatricDocs(root)
        val btn1 = root.findViewById<View>(R.id.phyciastry_btn) as ImageButton
        val btn2 = root.findViewById<View>(R.id.physcology_btn) as ImageButton
        btn1.setOnClickListener(){
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                var  name = "psychiastryDocButton"
                param(FirebaseAnalytics.Param.ITEM_ID, name)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "button")
            }
            psychiatricDocs(root)
        }
        btn2.setOnClickListener(){
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                var  name = "psycologyDocButton"
                param(FirebaseAnalytics.Param.ITEM_ID, name)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "button")
            }
            psycologyDocs(root)
        }
        return root
    }

private fun psychiatricDocs(root :View){
    docViewModel.getPsychiatryDocs().observe((activity as MainActivity),Observer<MutableList<Doctor>>{
        psychiatricDocs = it
        var adapter1 = DocsAdapter(requireContext(),psychiatricDocs)
        val list = root.findViewById<View>(R.id.doctors_list) as ListView
        list.adapter = adapter1
        list.setOnItemClickListener{parentFragment,view, position, id ->
            var  name = "doc_"+it[position].name+"_called"
            val intent = Intent(Intent.ACTION_CALL);
            var phone = it[position].phone.toString()
            intent.data = Uri.parse("tel:$phone")
            startActivity(intent)
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                param(FirebaseAnalytics.Param.ITEM_ID, name)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "doctor")
            }
        }
    })
}
    private fun psycologyDocs (root :View){

        docViewModel.getPsycologyDocs().observe((activity as MainActivity),Observer<MutableList<Doctor>>{
           psycologyDocs = it
            var adapter1 = DocsAdapter(requireContext(),psycologyDocs)
            val list = root.findViewById<View>(R.id.doctors_list) as ListView
            list.adapter = adapter1
            list.setOnItemClickListener{parentFragment,view, position, id ->
                val intent = Intent(Intent.ACTION_CALL);
                var phone = it[position].phone.toString()
                intent.data = Uri.parse("tel:$phone")
                var  name = "doc_"+it[position].name+"_called"
                startActivity(intent)
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                    param(FirebaseAnalytics.Param.ITEM_ID, name)
                    param(FirebaseAnalytics.Param.ITEM_NAME, name)
                    param(FirebaseAnalytics.Param.CONTENT_TYPE, "doctor")
                }

            }
        })
    }
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment fragment_doctors.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            fragment_doctors().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}