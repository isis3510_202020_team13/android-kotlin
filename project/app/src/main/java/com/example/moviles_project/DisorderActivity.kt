package com.example.moviles_project

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_disorder.*

class DisorderActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_disorder)
        val dis = intent.getSerializableExtra("disorder") as Disorder
        disorder_detail_title.text = dis.Name
        disorder_detail_description.text = dis.Description
        symptoms_dis_detail.text = dis.Symptoms
        treatment_dis_detail.text = dis.Treatment
    }
}