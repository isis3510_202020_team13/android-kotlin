package com.example.moviles_project

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [fragment_home.newInstance] factory method to
 * create an instance of this fragment.
 */
class fragment_home : Fragment() {
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
  
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        
        
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        firebaseAnalytics = Firebase.analytics
        val name =(activity as MainActivity).name1
        val root =  inflater.inflate(R.layout.fragment_home, container, false)
        val PillButton = root.findViewById<View>(R.id.Pill_button) as ImageButton
        var SchedulerButton = root.findViewById<View>(R.id.Scheduler_Button) as ImageButton
        var DisordersButton = root.findViewById<View>(R.id.Disorders_Button) as ImageButton
        var MedicationButton = root.findViewById<View>(R.id.medications_button) as ImageButton
        var HospitalButton = root.findViewById<View>(R.id.Hospital_Button) as ImageButton
        var doctorsButton = root.findViewById<View>(R.id.Doctor_Button) as ImageButton
        var text = root.findViewById<View>(R.id.App_greeting_text) as TextView
        text.setText("Hello \n" + name +"!").toString()

        PillButton.setOnClickListener {
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                var  name = "pillButton"
                param(FirebaseAnalytics.Param.ITEM_ID, name)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "button")
            }
            Navigation.findNavController(it).navigate(fragment_homeDirections.actionNavigationHomeToFragmentPillBox())
        }
        SchedulerButton.setOnClickListener {
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                var  name = "schedulerButton"
                param(FirebaseAnalytics.Param.ITEM_ID, name)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "button")
            }
            Navigation.findNavController(it).navigate(fragment_homeDirections.actionNavigationHomeToFragmentScheduler())
        }
        DisordersButton.setOnClickListener {
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                var  name = "disordersButton"
                param(FirebaseAnalytics.Param.ITEM_ID, name)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "button")
            }
            Navigation.findNavController(it).navigate(fragment_homeDirections.actionNavigationHomeToFragmentDisorder())
        }
        MedicationButton.setOnClickListener {
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                var  name = "medicationButton"
                param(FirebaseAnalytics.Param.ITEM_ID, name)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "button")
            }
            Navigation.findNavController(it).navigate(fragment_homeDirections.actionNavigationHomeToMedications())

        }
        HospitalButton.setOnClickListener {
            (activity as MainActivity).obtieneLocalizacion()
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                var  name = "hospitalButton"
                param(FirebaseAnalytics.Param.ITEM_ID, name)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "button")
            }
            Navigation.findNavController(it).navigate(fragment_homeDirections.actionNavigationHomeToFragmentHospitals2())

        }
        doctorsButton.setOnClickListener {
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                var  name = "doctorsButton"
                param(FirebaseAnalytics.Param.ITEM_ID, name)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "button")
            }
            Navigation.findNavController(it).navigate(fragment_homeDirections.actionNavigationHomeToFragmentDoctors())
            }
        return root

    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment fragment_home.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            fragment_home().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
        
        
    }
    
    
}