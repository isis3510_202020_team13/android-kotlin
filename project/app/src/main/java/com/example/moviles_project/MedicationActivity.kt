package com.example.moviles_project

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_medication.*

class MedicationActivity: AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_medication)
        val med = intent.getSerializableExtra("medication") as Medication
        med_detail_title.text = med.nameShort
        med_detail_description.text = med.Description
        adverse_med_detail.text = med.Adverse
}
}