package com.example.moviles_project

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import android.widget.Button

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [fragment_user_sents.newInstance] factory method to
 * create an instance of this fragment.
 */
class fragment_user_sents : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var firebaseAnalytics: FirebaseAnalytics


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun showBasicDialog(view: View?, feed: String?) {
        val builder = AlertDialog.Builder(this.context)
        if (feed != null){
            builder.setTitle("Notification")
            builder.setMessage("Feedback to " + feed + " sent successfully!")
            builder.show()
        }else{
            builder.setTitle("Notification")
            builder.setMessage("Thank you for your feedback :)")
            builder.show()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        firebaseAnalytics = Firebase.analytics
        val root = inflater.inflate(R.layout.fragment_user_sents, container, false)
        var sDocButton = root.findViewById<View>(R.id.submit_doctor_button) as Button
        var sFriendButton = root.findViewById<View>(R.id.submit_friend_button) as Button
        var sFeedbackButton = root.findViewById<View>(R.id.submit_feedback_button) as Button
        var sAll = root.findViewById<View>(R.id.submit_all_button) as Button

        sDocButton.setOnClickListener {
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                var  name = "sendFeedback2Doctors"
                param(FirebaseAnalytics.Param.ITEM_ID, name)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "button")
            }
            showBasicDialog(null, "Doctors")
            Navigation.findNavController(it).navigate(fragment_user_sentsDirections.actionNavigationUserSentsToHome())
        }

        sFriendButton.setOnClickListener {
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                var  name = "sendFeedback2Friends"
                param(FirebaseAnalytics.Param.ITEM_ID, name)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "button")
            }
            showBasicDialog(null, "Friends")
            Navigation.findNavController(it).navigate(fragment_user_sentsDirections.actionNavigationUserSentsToHome())
        }

        sFeedbackButton.setOnClickListener {
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                var  name = "sendFeedback2Us"
                param(FirebaseAnalytics.Param.ITEM_ID, name)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "button")
            }
            showBasicDialog(null, null)
            Navigation.findNavController(it).navigate(fragment_user_sentsDirections.actionNavigationUserSentsToHome())
        }

        sAll.setOnClickListener {
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                var  name = "sendFeedback2Doctors"
                param(FirebaseAnalytics.Param.ITEM_ID, name)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "button")

                name = "sendFeedback2Friends"
                param(FirebaseAnalytics.Param.ITEM_ID, name)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "button")

                name = "sendFeedback2Us"
                param(FirebaseAnalytics.Param.ITEM_ID, name)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "button")
            }
            showBasicDialog(null, null)
            Navigation.findNavController(it).navigate(fragment_user_sentsDirections.actionNavigationUserSentsToHome())
        }
        return root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment fragment_user_sents.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            fragment_user_sents().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}