package com.example.moviles_project

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.item_disorder.view.*

class DisAdapter(private val mContext: Context, private val listDis:List<Disorder>) : ArrayAdapter<Disorder>(mContext,0,listDis) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layout = LayoutInflater.from(mContext).inflate(R.layout.item_disorder,parent,false)
        val disorder = listDis[position]
        layout.dis_name.text = disorder.Name
        return layout
    }
}