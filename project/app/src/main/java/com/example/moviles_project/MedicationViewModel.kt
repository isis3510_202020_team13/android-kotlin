package com.example.moviles_project

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ListView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class MedicationViewModel:ViewModel() {
    var database = FirebaseDatabase.getInstance()
    var med = database.getReference("Medications");
    private val medications: MutableLiveData<MutableList<Medication>> by lazy {
        MutableLiveData<MutableList<Medication>>().also {
            loadMedication()
        }
    }

    fun getmedications(): LiveData<MutableList<Medication>> {
        return medications
    }

    private fun loadMedication(){
        med.addValueEventListener( object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var list:MutableList<Medication> = mutableListOf<Medication>()
                if (dataSnapshot.exists()) run {
                    val data = dataSnapshot.children!!
                    data.forEach{
                        var dat= it.getValue() as Map<String,Any>
                        var med = Medication(dat["Name"].toString(),dat["Description"].toString(),dat["Adverse"].toString(),dat["NameShort"].toString())
                        list.add(med)
                        medications.postValue(list)

                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Failed to read value
            }
        })

    }




}