package com.example.moviles_project

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.item_medication.view.*

class MedsAdapter(private val mContext:Context,private val listMeds:List<Medication>) : ArrayAdapter<Medication>(mContext,0,listMeds) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layout = LayoutInflater.from(mContext).inflate(R.layout.item_medication,parent,false)
         val medication = listMeds[position]
        layout.med_name.text = medication.Name
        return layout
    }

}