package com.example.moviles_project

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.item_doctor.view.*

class DocsAdapter(private val mContext: Context, private val listDocs:List<Doctor>) : ArrayAdapter<Doctor>(mContext,0,listDocs) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layout = LayoutInflater.from(mContext).inflate(R.layout.item_doctor,parent,false)
        val doctor = listDocs[position]
        layout.doctor_name.text = doctor.name
        layout.doctor_type.text = doctor.type
        layout.rating_text.text = doctor.rating.toString()
        return layout
    }

}