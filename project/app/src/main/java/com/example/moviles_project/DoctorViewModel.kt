package com.example.moviles_project
import androidx.lifecycle.ViewModel
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.moviles_project.Doctor
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class DoctorViewModel:ViewModel() {
    var database = FirebaseDatabase.getInstance()
    var psycology = database.getReference("Doctors/Phycology");
    var psychiatry = database.getReference("Doctors/Psychiatry");
    private val psychiatryDocs: MutableLiveData<MutableList<Doctor>> by lazy {
        MutableLiveData<MutableList<Doctor>>().also {
            loadPsychiatryDocs()
        }
    }

    private val psycologyDocs: MutableLiveData<MutableList<Doctor>> by lazy {
        MutableLiveData<MutableList<Doctor>>().also {
            loadPsycologyDocs()
        }
    }

    fun getPsychiatryDocs(): LiveData<MutableList<Doctor>> {

        return psychiatryDocs
    }

    fun getPsycologyDocs(): LiveData<MutableList<Doctor>> {

        return psycologyDocs
    }

    private fun loadPsychiatryDocs() {
        psychiatry.addValueEventListener( object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var list:MutableList<Doctor> = mutableListOf<Doctor>()
                if (dataSnapshot.exists()) run {
                    val data = dataSnapshot.children!!
                    data.forEach {
                        var dat = it.getValue() as Map<String, Any>
                        var doc = Doctor(dat["Name"] as String,dat["Type"] as String,dat["Rating"]as Number, "a",dat["Phone"] as Number)
                        list.add(doc)
                        psychiatryDocs.postValue(list)
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Failed to read value
            }

        }
        )
    }

    private fun loadPsycologyDocs() {
        psycology.addValueEventListener( object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var list:MutableList<Doctor> = mutableListOf<Doctor>()
                if (dataSnapshot.exists()) run {
                    val data = dataSnapshot.children!!
                    data.forEach {
                        var dat = it.getValue() as Map<String, Any>
                        var doc = Doctor(dat["Name"] as String,dat["Type"] as String,dat["Rating"]as Number, "a",dat["Phone"] as Number)
                        list.add(doc)
                        psycologyDocs.postValue(list)
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Failed to read value
            }

        }
        )
    }

}